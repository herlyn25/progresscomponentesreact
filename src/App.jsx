import { useState } from 'react'
import './App.css'

function Progress1(){ 
  const style={
    width: '50%'
  } 
  return (
  <div className="progress">
    <div className="progress-bar" role="progressbar" style={style} aria-valuenow="50"aria-valuemin="0" aria-valuemax="100"></div><br />   <br />    
  </div>  
  )
}
function ProgressLabel(props){  
  return (
  <div className="progress" onClick={()=>{
    props.porcentaje+=5
    console.log(props.porcentaje)}}>
    <div  className="progress-bar" role="progressbar" style={{width:props.porcentaje ? props.porcentaje+"%" : "100%"}}
     aria-valuenow={props.width} aria-valuemin="0" aria-valuemax="100">{props.porcentaje}%
    
    </div>
   
  </div>
  )
}
function ProgressHeight(){
  const style={
    width:"48%" 
  } 
  return (
  <div className="progress" style="height: 1px;">
    <div className="progress-bar" role="progressbar" style={style} aria-valuenow="48" aria-valuemin="0" aria-valuemax="100"></div>
  </div>);
}
function ProgressColor(){
  const style={
    width:"60%"
  }
  return (
  <div className="progress">
    <div className="progress-bar bg-info" role="progressbar" style={style} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
  </div>)
}
function ProgressVariosColores(){
  const style={
      width:"15%"      
  }
  const style2={
    width:"30%"      
  }
  const style3={
    width:"20%"      
  }
  return (    
    <div class="progress">
      <div class="progress-bar" role="progressbar" style={style} aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
      <div class="progress-bar bg-success" role="progressbar" style= {style2} aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
      <div class="progress-bar bg-info" role="progressbar" style={style3} aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  )
}
function ProgressRayas(){
  const style={
    width:650
  }
  return (
  <div className="progress">
    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={style} aria-valuenow={style.width} aria-valuemin="0" aria-valuemax="100"></div>
  </div>)
} 
function ProgressRayasLabel(){
  const style={
    width:975
  }
  return (
  <div className="progress">
    <div className="progress-bar progress-bar-striped bg-warning" role="progressbar" style={style} aria-valuenow={style.width} aria-valuemin="0" aria-valuemax="100">{style.width*100/1300}%</div>
  </div>)
} 
function ProgressRayasAnimadaLabel(){
  const style={
    width:800
  }
  return (
  <div className="progress">
    <div className="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style={style} aria-valuenow={style.width} aria-valuemin="0" aria-valuemax="100">{Math.trunc(style.width*100/1300)}%</div>
  </div>)
} 
function Nombres(props){
return (
  <h3>Barra {props.name}</h3>
)
}

function App() {
  return (
    <div className="container">
      <header>  <br />  
       <h1 > Barras de progresso</h1> 
       <Nombres name="Simple"/>   
       <Progress1 /> <br />
       <Nombres name="con etiqueta de porcentaje"/>    
       <ProgressLabel porcentaje={30}/>      
      
       <Nombres name="simple con color"/>   
       <ProgressColor /> <br />
       <Nombres name="simple con varios colores"/>  
       <ProgressVariosColores/><br />
       <Nombres name="Rayada"/>    
       <ProgressRayas/><br />
       <Nombres name="Rayada con etiqueta de porcentaje"/>  
       <ProgressRayasLabel /> <br /> 
       <Nombres name="Rayada con etiqueta de porcentaje y animada"/>   
       <ProgressRayasAnimadaLabel />
      </header>
    </div>
  )
}

export default App
